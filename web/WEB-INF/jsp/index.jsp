<%@page import="Model.RssChannel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
<% 
    RssChannel channel = (RssChannel)request.getAttribute("channel");
    if(channel==null)
    {
        channel = new RssChannel();
    }
%>    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <form action="RssController" method="post">
            Enter RSS Feed: <input type="text" name="url" value="http://www.nasa.gov/rss/dyn/breaking_news.rss"><br>
            <input type="submit" value="Submit">
        </form>
        URL: <%out.print(channel.getUrl());%><br>
        Title: <%out.print(channel.getTitle());%><br>
        Link: <%out.print(channel.getLink());%><br>
        <%out.print(channel.getDescription());%><br>
        <br>
        <%
            for(int i=0;i<channel.getItems().size();i++)
            {
                out.print("<b>" + channel.getItems().get(i).getTitle() + "</b>" + channel.getItems().get(i).getPubDate() + "<br>");
                out.print("<i>" + channel.getItems().get(i).getLink() + "</i><br>");
                out.print(channel.getItems().get(i).getDescription() + "<br><br>");
            }
        %>
    </body>
</html>
