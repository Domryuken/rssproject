package Model;

import Model.RssItem;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class RssChannel
{

    private String url = "";
    private String title = "";
    private String link = "";
    private String description = "";
    private ArrayList<RssItem> items = new ArrayList<>();

    /**
     * Url is set and then the channel is updated to populate it with the
     * correct data
     *
     * @param givenUrl
     */
    public RssChannel()
    {
    }

    //getters
    public String getUrl()
    {
        return url;
    }

    public String getTitle()
    {
        return title;
    }

    public String getLink()
    {
        return link;
    }

    public String getDescription()
    {
        return description;
    }

    public ArrayList<RssItem> getItems()
    {
        return items;
    }

    //setters
    public void setUrl(String givenUrl)
    {
        url = givenUrl;
    }

    public void setTitle(String givenTitle)
    {
        title = givenTitle;
    }

    public void setLink(String givenLink)
    {
        link = givenLink;
    }

    public void setDescription(String givenDescription)
    {
        description = givenDescription;
    }

    @Override
    public String toString()
    {
        String string = title + "\n" + link + "\n" + description + "\n";
        for (int i = 0; i < items.size(); i++)
        {
            string = string + "\n" + items.get(i).toString();
        }
        return string;
    }

    public void update() throws IOException
    {
        Document channelDoc = Jsoup.connect(url).get();
        Elements itemElements = channelDoc.select("rss > channel > item").remove();
        title = channelDoc.select("rss > channel > title").text();
        link = channelDoc.select("rss > channel > link").text();
        description = channelDoc.select("rss > channel > description").text();
        for (Element e : itemElements)
        {
            RssItem thisItem = new RssItem();
            thisItem.setTitle(e.select("title").text());
            thisItem.setLink(e.select("link").text());
            thisItem.setDescription(e.select("description").text());
            thisItem.setPubDate(e.select("pubdate").text());
            items.add(thisItem);
        }
    }
    

}
