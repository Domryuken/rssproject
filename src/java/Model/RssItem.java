package Model;






public class RssItem
{
    private String title;
    private String link;
    private String description;
    private String pubdate;
    
    /**
     * Takes 4 strings title, link, description, and publication date
     * @param givenTitle
     * @param givenLink
     * @param givenDescription
     * @param givenPubDate 
     */
    public RssItem(){}
    
    //Getters
    public String getTitle(){return title;}
    public String getLink(){return link;}
    public String getDescription(){return description;}
    public String getPubDate(){return pubdate;}
    
    //Setters
    public void setTitle(String givenTitle){title = givenTitle;}
    public void setLink(String givenLink){link = givenLink;}
    public void setDescription(String givenDescription){description = givenDescription;}
    public void setPubDate(String givenPubDate){pubdate = givenPubDate;}
    
    //toString Method to give all data as a single string
    @Override
    public String toString(){return title + "\n" + link + "\n" + description + "\n" + pubdate;}
    
}
